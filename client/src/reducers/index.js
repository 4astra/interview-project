import { combineReducers } from 'redux';
import { REQUEST_USERS, RECEIVE_USERS, REQUEST_SEARCH, RECEIVE_SEARCH } from '../actions';

const userReducer = (state = { users: [] }, action) => {
  switch (action.type) {
    case REQUEST_USERS:
      return {
        ...state,
        isFetching: false,
      }
    case RECEIVE_USERS:
      return {
        ...state,
        isFetching: true,
        users: action.users,
      }
    default:
      return state
  }
}
const searchReducer = (state = { keyword: '', results: [] }, action) => {
  switch (action.type) {
    case REQUEST_SEARCH:
      return {
        ...state,
        isFetching: false,
      }
    case RECEIVE_SEARCH:
      return {
        ...state,
        isFetching: true,
        results: action.results,
      }
    default:
      return state
  }
}

const rootReducer = combineReducers({
  userReducer,
  searchReducer
})

export default rootReducer;