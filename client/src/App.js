import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ListUser from './components/ListUser';
import { fetchUsers, fetchSearch } from './actions'
import _ from 'lodash';

class App extends Component {
  static propTypes = {
    users: PropTypes.array.isRequired,
    results: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired,

  }
  
  constructor(props) {
    super(props);

    this.textChange = this.textChange.bind(this)
  }

  // state = {
  //   response: []
  // };

  // componentDidMount() {
  //   this.callApi()
  //     .then(res => {
  //       this.setState({ response: res.express })
  //     })
  //     .catch(err => console.log(err));
  // }

  // callApi = async () => {
  //   const response = await fetch('/api/hello');
  //   const body = await response.json();

  //   if (response.status !== 200) throw Error(body.message);

  //   return body;
  // };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchUsers());
  }

  textChange(e) {
    const keyword = e.target.value;

    if (keyword !== '' && keyword !== undefined) {
      console.log('Im here')
      this.props.dispatch(fetchSearch(keyword))
    }
  }

  render() {
    const { users, results } = this.props;
    console.log('A:', users);
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        {/* <ListUser users={this.state.response}/> */}
        <input type="text" onChange={
          this.textChange
        } placeholder="Enter a keyword to search" />
        <ListUser users={results} />
      </div>
    );
  }
}

// export default App;

const mapStateToProps = state => {
  const { userReducer, searchReducer } = state

  return {
    users: userReducer.users,
    results: searchReducer.results
  }
}

export default connect(mapStateToProps)(App)
