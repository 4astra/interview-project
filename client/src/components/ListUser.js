import React from 'react'
import PropTypes from 'prop-types'

const ListUser = ({ users }) => (

  <table className="table App-intro">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Email</th>
        <th scope="col">Job Title</th>
        <th scope="col">Phone</th>
      </tr>
    </thead>
    <tbody>
      {
        users.map(e => (
          <tr key={e.id}>
            <th scope="row">{e.id}</th>
            <td>{e.firstName}</td>
            <td>{e.lastName}</td>
            <td>{e.email}</td>
            <td>{e.jobTitle}</td>
            <td>{e.phone}</td>
          </tr>
        ))
      }
    </tbody>
  </table>
)

ListUser.propTypes = {
  users: PropTypes.array.isRequired
}

export default ListUser;