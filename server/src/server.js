const express = require('express');
const csv = require('csvtojson');
var path = require('path');

const csvPath = path.join(__dirname, '../../', 'data', 'users.csv');

const app = express();
const port = process.env.PORT || 5000;

app.get('/api/hello', (req, res) => {

  readCSV(csvPath).then(data => {
    res.send({ express: data });
  }).catch(e => {
    res.send({ express: [] });
    console.log('Error:', e);
  });

});

app.get('/api/search', (req, res) => {
  console.log('Receive: ', req.query.q);
  
  
  
  const keyword = req.query.q.toLowerCase();

  readCSV(csvPath).then(data => {
    const a = data.map(e => {
      let tempPhone = e.phone.replace(/-/g, '');
      return {
        ...e,
        "tempPhone": tempPhone
      }
    })
    const result = a.filter(e => {
      return e.firstName.toLowerCase().includes(keyword) 
      || e.lastName.toLowerCase().includes(keyword) 
      || e.email.includes(keyword) || e.phone.includes(keyword) || e.tempPhone.includes(keyword);
    })
    res.send({ express: result });
  }).catch(e => {
    res.send({ express: [] });
    console.log('Error:', e);
  });
  
});

if (process.env.NODE_ENV !== 'test') {
  app.listen(port, () => console.log(`Listening on port ${port}`));
}

async function readCSV(path) {
  return csv()
    .fromFile(path)
    .then((jsonObj) => {
      return jsonObj;
    });
}

module.exports = app;
